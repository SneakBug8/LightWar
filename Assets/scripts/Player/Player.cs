﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Player : Entity
{
    public static Player Global;

    void Awake()
    {
        Global = this;
    }

    protected override void Start()
    {
        base.Start();
        InitializeUI();
    }

    void Update()
    {
        transform.Translate(
            new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical")).normalized * Characteristics.Speed * Time.deltaTime
        );

        if (Input.GetButtonDown("Fire1"))
        {
            ShootRay(Camera.main.ScreenToWorldPoint(Input.mousePosition));
        }

        UpdateUI();
    }

    #region LightRay
    [Header("LightRay")]
    public GameObject LightRayPrefab;
    Rigidbody2D LightRayInstance;
    public float LightRaySpeed = 500;
    void ShootRay(Vector2 destination)
    {
        if (LightRayInstance != null)
        {
            return;
        }
        var raybullet = Instantiate(LightRayPrefab, transform.position, Quaternion.identity);
        LightRayInstance = raybullet.GetComponent(typeof(Rigidbody2D)) as Rigidbody2D;
        Vector2 direction = PointToDirection(destination);
        LightRayInstance.AddForce(direction * LightRaySpeed);
    }

    public void TeleportTo(Vector2 destination)
    {
         transform.position = destination;
    }
    Vector2 PointToDirection(Vector2 destination)
    {
        Vector2 direction = transform.InverseTransformPoint(destination); //- transform.position;
        direction.Normalize();

        return direction;
    }
    #endregion

    #region UI
    public Slider HealthBar;
    public void InitializeUI()
    {
        if (HealthBar != null)
        {
            HealthBar.minValue = 0;
            HealthBar.maxValue = Characteristics.MaxHealth;
        }
        else
        {
            Debug.LogWarning("No HealthBar set!");
        }
    }
    public void UpdateUI()
    {
        //HealthBar
        if (HealthBar != null)
        {
            HealthBar.value = Characteristics.Health;
        }
        else
        {
            Debug.LogWarning("No HealthBar set!");
        }
    }
    #endregion
}
