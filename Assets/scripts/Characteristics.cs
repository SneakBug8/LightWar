﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public class Characteristics {
	public UnityEvent OnDeath;

	public int Health {
		get {
			return _health;
		}
		set {
			_health = value;
			if (_health <= 0) {
				OnDeath.Invoke();
			}
		}
	}
	[SerializeField]
	int _health;
	public int MaxHealth;
	public int Damage;

	// Used only by AI
	public float TimeBetweenAttacks;

	public float Speed;

	public void MultiplyBy(Characteristics multiplier) {
		Health *= multiplier.Health;
		Damage *= multiplier.Damage;
		Speed *= multiplier.Speed;
	}
}
