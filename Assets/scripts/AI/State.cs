using UnityEngine;

public class State : MonoBehaviour {
    public AI Owner;
    public void SetOwner(AI owner) {
        Owner = owner;
    }

    public virtual void Update() {}

    public virtual void OnDamage() {}

    public virtual void OnEnable() {}

    public virtual void OnDisable() {}
}