using UnityEngine;

public class Bullet : MonoBehaviour
{
    public int Damage;
    public GameObject HurtEffect;
    private void OnTriggerEnter2D(Collider2D other)
    {
        var entity = other.gameObject.GetComponent<Entity>();
        if (entity != null)
        {
            entity.Characteristics.Health -= Damage;
        }

        Instantiate(HurtEffect, transform.position, Quaternion.identity);
        Destroy(gameObject);        
    }
}