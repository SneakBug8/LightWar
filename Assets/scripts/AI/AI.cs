﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AI : Entity {
	public State State;
	public State PreviousState;
	public float ViewDistance;

	protected override void Start() {
		base.Start();
		ChangeState(State);
	}
	public void ChangeState(State state) {
		PreviousState = State;
		State.enabled = false;
		state.enabled = true;
		State = state;
		State.SetOwner(this);
	}
}
