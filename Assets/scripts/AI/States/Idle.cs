using UnityEngine;
public class Idle : State {
    public override void OnEnable() {
        // Stop moving
    }
    public override void Update() {
        if (Vector3.Distance(transform.position, Player.Global.transform.position) < Owner.ViewDistance) {
            Owner.ChangeState(new Attack());
        }
    }
}