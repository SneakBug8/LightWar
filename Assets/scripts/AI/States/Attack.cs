using UnityEngine;

public class Attack : State
{
    float attackTimeout;
    public GameObject BulletPref;
    public float BulletSpeed = 500;
    public override void Update()
    {
        if (Vector3.Distance(Owner.transform.position, Player.Global.transform.position) > Owner.ViewDistance)
        {
            Owner.ChangeState(Owner.PreviousState);
        }

        attackTimeout -= Time.deltaTime;

        if (attackTimeout <= 0)
        {
            var hit = Physics2D.Raycast(transform.TransformPoint(PointToDirection(Player.Global.transform.position)), PointToDirection(Player.Global.transform.position)); //, Owner.ViewDistance, 6);
            if (hit.collider != null && hit.collider.gameObject == Player.Global.gameObject)
            {
                attackTimeout = Owner.Characteristics.TimeBetweenAttacks;
                Shoot(Player.Global.transform.position);
            }
        }
        // Move towards Player
    }

    void Shoot(Vector2 destination)
    {
        if (BulletPref == null)
        {
            return;
        }
        Debug.Log("Shoot");
        var bullet = Instantiate(BulletPref, transform.position, Quaternion.identity);
        var BulletRigidbody = bullet.GetComponent(typeof(Rigidbody2D)) as Rigidbody2D;
        Vector2 direction = PointToDirection(destination);
        bullet.GetComponent<Bullet>().Damage = Owner.Characteristics.Damage;
        BulletRigidbody.AddForce(direction * BulletSpeed);
    }

    Vector2 PointToDirection(Vector2 destination)
    {
        Vector2 direction = transform.InverseTransformPoint(destination); //- transform.position;
        direction.Normalize();

        return direction;
    }
}