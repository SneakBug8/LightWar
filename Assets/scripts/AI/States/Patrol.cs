using UnityEngine;

public class Patrol : State {
    public Vector3[] Points;
    public int CurrentPoint;

    private void OnDrawGizmosSelected() {
        if (Points.Length == 0) {
            return;
        }

        Gizmos.color = Color.red;
        var lastpos = transform.position;
        foreach (var point in Points) {
            Gizmos.DrawWireSphere(point, 0.5f);
            Gizmos.DrawLine(lastpos, point);
            lastpos = point;
        }

        Gizmos.DrawLine(lastpos, Points[0]);
    }
    public override void Update() {
        // Move towards currentpoint
        if (Vector3.Distance(Owner.transform.position, Points[CurrentPoint]) < 1f) {
            CurrentPoint = (CurrentPoint + 1) % Points.Length;
        }

        if (Vector3.Distance(transform.position, Player.Global.transform.position) < Owner.ViewDistance) {
            var attack = Owner.GetComponent<Attack>();
            if (attack != null) {
                Owner.ChangeState(attack);
            }
        }
    }
}