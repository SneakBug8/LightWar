using UnityEngine;

public class Entity : MonoBehaviour {
    public Characteristics Characteristics;

    protected virtual void Start() {
        Characteristics.OnDeath.AddListener(Die);
    }

    protected virtual void Die() {
        Destroy(gameObject);
    }
}