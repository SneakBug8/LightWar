using UnityEngine;

public class LightRay : MonoBehaviour {

    Vector2 startingPoint;
    public GameObject HurtEffect;

    public float MaxDistance = 40;
    private void Start() {
        startingPoint = transform.position;
    }

    private void Update() {
        if (Vector2.Distance(startingPoint,transform.position) >= MaxDistance) {
            Destroy(gameObject);
        }
    }
    private void OnTriggerEnter2D(Collider2D other) {
        var entity = other.gameObject.GetComponent<Entity>();
        if (entity == null) {
            Player.Global.TeleportTo(transform.position);
            Instantiate(HurtEffect, transform.position, Quaternion.identity);
            Destroy(gameObject);
        }
        else {
            entity.Characteristics.Health -= Player.Global.Characteristics.Damage;
            Instantiate(HurtEffect, transform.position, Quaternion.identity);            
        }
    }
}