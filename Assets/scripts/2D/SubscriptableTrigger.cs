﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(BoxCollider2D))]
public class SubscriptableTrigger : MonoBehaviour {

	public class Collider2DEvent : UnityEvent<Collider2D> {}

	public Collider2DEvent OnTriggerEnter;

	public void OnTriggerEnter2D (Collider2D collider) {
		OnTriggerEnter.Invoke (collider);
	}
}
