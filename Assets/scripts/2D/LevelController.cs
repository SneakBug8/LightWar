﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelController : MonoBehaviour {
	public static LevelController Global;

	void Awake() {
		Global = this;
	}

	void Start() {
		if (LevelEnd != null) {
			LevelEnd.OnTriggerEnter.AddListener (TryEndLevel);
		} else {
			Debug.LogWarning ("No LevelEnd set");
		}
	}

	#region Enemies
	[Space]
	[Header("Enemies")]
	[Tooltip("Each monster characteristics will be multiplied by this characteristics")]
	public Characteristics EnemiesCharacteristicsMultiplier;
	#endregion

	#region LevelEnding
	[Space]
	[Header("Level ending")]

	[Tooltip("Which scene load on LevelEnd")]
	public string NextLevel;

	[Tooltip("Trigger for ending level")]
	public SubscriptableTrigger LevelEnd;

	void TryEndLevel(Collider2D collider) {
		if (IsPlayer (collider)) {
			EndLevel ();
		}
	}

	bool IsPlayer(Collider2D collider) {
		if (collider.gameObject == Player.Global.gameObject) {
			return true;
		} else {
			return false;
		}
	}

	void EndLevel() {
		SceneManager.LoadScene (NextLevel);
	}

	#endregion
}
