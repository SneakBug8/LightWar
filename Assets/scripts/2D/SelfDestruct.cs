using UnityEngine;

public class SelfDestruct : MonoBehaviour {
    public float TimeToDestruct = 1f;

    private void Update() {
        TimeToDestruct -= Time.deltaTime;

        if (TimeToDestruct <= 0) {
            Destroy(gameObject);
        }
    } 
}