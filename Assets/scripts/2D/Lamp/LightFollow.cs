using UnityEngine;

public class LightFollow : MonoBehaviour {
    public Entity Target;
    public float Speed;
    public int Health;
    private void Update() {
        var direction = PointToDirection(Target.transform.position);
        transform.Translate(direction * Speed * Time.deltaTime);

        if (Vector3.Distance(transform.position, Target.transform.position) < 0.5f) {
            Target.Characteristics.Health += Health;
            Destroy(gameObject);
        }
    }

    Vector2 PointToDirection(Vector2 destination)
    {
        Vector2 direction = transform.InverseTransformPoint(destination); //- transform.position;
        return direction.normalized;
    }
}