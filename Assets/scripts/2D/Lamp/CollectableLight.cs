﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[RequireComponent(typeof(CircleCollider2D))]
public class CollectableLight : MonoBehaviour {
	public int Health = 20;
	public GameObject LightFollowPref;
	private void OnTriggerEnter2D(Collider2D other) {
		var collector = other.GetComponent<LightCollector>();
		if (collector != null) {
			var instance = Instantiate(LightFollowPref, transform.position, Quaternion.identity);
			var LightFollowInstance = instance.GetComponent<LightFollow>();

			LightFollowInstance.Target = collector.GetComponent<Entity>();
			LightFollowInstance.Health = Health;

			Destroy(gameObject);
		}
	}
}
